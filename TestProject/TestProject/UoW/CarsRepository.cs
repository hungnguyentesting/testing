﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestProject.Models;
using System.Data.Entity;

namespace TestProject.UoW
{
    public class CarsRepository
    {
        private CarsEntities _entities = new CarsEntities();
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public CarsRepository(CarsEntities entities)
        {
            _entities = entities;
        }
        public void Add(Car obj)
        {
            try
            {
                _entities.Cars.Add(obj);
                logger.Warn("Added new Car: " + obj.CarID);
            }
            catch (Exception e)
            {
                logger.Error("Error when add new Car: " + obj.CarID, e);
                throw;
            }
            
        }
        public void Delete(Car obj)
        {
            try
            {
                _entities.Cars.Remove(obj);
                logger.Warn("Deleted Car: " + obj.CarID);
            }
            catch (Exception e)
            {
                logger.Error("Error when delete Car: " + obj.CarID, e);
                throw;
            }
           
        }
        public void SaveChage()
        {
            try
            {
                _entities.SaveChanges();

            }
            catch (Exception e)
            {
                logger.Error("Error when save the db", e);
                throw;
            }
        }
        public List<Car> GetAllCars()
        {
            try
            {
                return _entities.Cars.ToList();

            }
            catch (Exception e)
            {
                logger.Error("Error when get all cars", e);
                return null;
            }
        }
        public Car GetById(int id)
        {
            try
            {
                return _entities.Cars.Where(c => c.CarID == id).FirstOrDefault();
            }
            catch (Exception e)
            {
                logger.Error("Error when get car by id", e);
                return null;
            }
        }
        public List<Car> SearchByModelOrBrand(string key)
        {
            try
            {
                return _entities.Cars.Where(c => c.Brand.Contains(key) || c.Model.Contains(key)).ToList();
            }
            catch (Exception e)
            {
                logger.Error("Error when search car by key", e);
                throw;
            }
        }
    }
}