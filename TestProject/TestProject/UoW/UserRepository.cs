﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestProject.Models;
using System.Data.Entity;

namespace TestProject.UoW
{
    public class UserRepository
    {
        private CarsEntities _entities = new CarsEntities();
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public UserRepository(CarsEntities entities)
        {
            _entities = entities;
        }
        public void Add(User obj)
        {
            try
            {
                _entities.Users.Add(obj);
                logger.Warn("Added new User: " + obj.UserID);
            }
            catch (Exception e)
            {
                logger.Error("Error when add new User: " + obj.UserID, e);
                throw;
            }
            
        }
        public void Delete(User obj)
        {
            try
            {
                _entities.Users.Remove(obj);
                logger.Warn("Deleted User: " + obj.UserID);
            }
            catch (Exception e)
            {
                logger.Error("Error when delete User: " + obj.UserID, e);
                throw;
            }
           
        }
        public void SaveChage()
        {
            try
            {
                _entities.SaveChanges();

            }
            catch (Exception e)
            {
                logger.Error("Error when save the db", e);
                throw;
            }
        }
        public List<User> GetAllUsers()
        {
            try
            {
                return _entities.Users.ToList();

            }
            catch (Exception e)
            {
                logger.Error("Error when get all Users", e);
                return null;
            }
        }
        public User GetById(int id)
        {
            try
            {
                return _entities.Users.Where(c => c.UserID == id).FirstOrDefault();
            }
            catch (Exception e)
            {
                logger.Error("Error when get User by id", e);
                return null;
            }
        }
        public User GetByName(string name)
        {
            try
            {
                return _entities.Users.Where(c => c.Username == name).FirstOrDefault();
            }
            catch (Exception e)
            {
                logger.Error("Error when get User by name", e);
                return null;
            }
        }

        public Inventory GetInventoryByUserID(int UserID)
        {
            try
            {
                return _entities.Inventories.Where(c => c.UserID == UserID).FirstOrDefault();
            }
            catch (Exception e)
            {
                logger.Error("Error when get Inventory by UserId", e);
                return null;
            }
        }
    }
}