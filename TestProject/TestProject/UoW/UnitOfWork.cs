﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestProject.Models;

namespace TestProject.UoW
{
    public class UnitOfWork
    {
        private CarsRepository _carRepo;
        private UserRepository _userRepo;
        private InventoryRepository _inventoryRepo;
        private CarsEntities _entities;
        public UnitOfWork(CarsEntities Entities)
        {
            _entities = Entities;
        }
        public CarsRepository CarsRepo
        {
            get
            {
                if (_carRepo == null)
                    _carRepo = new CarsRepository(_entities);
                return _carRepo;
            }
        }
        public UserRepository UsersRepo
        {
            get
            {
                if (_userRepo == null)
                    _userRepo = new UserRepository(_entities);
                return _userRepo;
            }
        }
        public InventoryRepository InventoryRepo
        {
            get
            {
                if (_inventoryRepo == null)
                    _inventoryRepo = new InventoryRepository(_entities);
                return _inventoryRepo;
            }
        }
        public void Save()
        {
            _entities.SaveChanges();
        }
    }
}