﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestProject.Models;
using System.Data.Entity;

namespace TestProject.UoW
{
    public class InventoryRepository
    {
        private CarsEntities _entities = new CarsEntities();
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public InventoryRepository(CarsEntities entities)
        {
            _entities = entities;
        }
        public void Add(Inventory obj)
        {
            try
            {
                _entities.Inventories.Add(obj);
                logger.Warn("Added new Inventory: " + obj.InventoryID);
            }
            catch (Exception e)
            {
                logger.Error("Error when add new Inventory: " + obj.InventoryID, e);
                throw;
            }
            
        }
        public void Delete(Inventory obj)
        {
            try
            {
                _entities.Inventories.Remove(obj);
                logger.Warn("Deleted Inventory: " + obj.InventoryID);
            }
            catch (Exception e)
            {
                logger.Error("Error when delete Inventory: " + obj.InventoryID, e);
                throw;
            }
           
        }
        public void AddCarToInventory(int carID, int UserID)
        {
            try
            {
                Inventory obj = new Inventory();
                obj.CarID = carID;
                obj.UserID = UserID;
                _entities.Inventories.Add(obj);
                logger.Warn("Added new Inventory:: " + obj.InventoryID);
            }
            catch (Exception e)
            {
                logger.Error("Error when add Inventory: ", e);
                throw;
            }

        }
        public void SaveChage()
        {
            try
            {
                _entities.SaveChanges();

            }
            catch (Exception e)
            {
                logger.Error("Error when save the db", e);
                throw;
            }
        }
        public List<Inventory> GetAllInventories()
        {
            try
            {
                return _entities.Inventories.ToList();

            }
            catch (Exception e)
            {
                logger.Error("Error when get all Inventories", e);
                return null;
            }
        }
        public Inventory GetByCarIDAndUserID(int Carid, int UserID)
        {
            try
            {
                return _entities.Inventories.Where(c => c.CarID == Carid && c.UserID == UserID).FirstOrDefault();
            }
            catch (Exception e)
            {
                logger.Error("Error when get Inventory by carID and UserID", e);
                return null;
            }
        }

        public List<Car> GetCarsByuserID(int id)
        {
            try
            {
                return _entities.Cars.Where(c => c.CarID == id).ToList();
            }
            catch (Exception e)
            {
                logger.Error("Error when get cars by userID", e);
                return null;
            }
        }
    }
}