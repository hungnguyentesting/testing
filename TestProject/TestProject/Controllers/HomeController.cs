﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestProject.UoW;
using TestProject.Models;

namespace TestProject.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            UnitOfWork _UoW = new UnitOfWork(new CarsEntities());
            var lst = _UoW.CarsRepo.GetAllCars();
            return View(lst);
        }
        [HttpPost]
        public ActionResult AddCar(FormCollection objfrm)
        {
            UnitOfWork _UoW = new UnitOfWork(new CarsEntities());
            Car obj = new Car();
            obj.Brand = objfrm["brand"];
            obj.Model = objfrm["model"];
            obj.Year = Convert.ToInt32(objfrm["year"]);
            obj.Price = Convert.ToDecimal(objfrm["price"]);
            obj.New = objfrm["new"].ToString() == "on";
            _UoW.CarsRepo.Add(obj);
            _UoW.Save();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult UpdateCar(FormCollection objfrm)
        {
            UnitOfWork _UoW = new UnitOfWork(new CarsEntities());
            Car obj = new Car();
            int id = Convert.ToInt32(objfrm["carId"]);
            obj = _UoW.CarsRepo.GetById(id);
            obj.Brand = objfrm["brand"];
            obj.Model = objfrm["model"];
            obj.Year = Convert.ToInt32(objfrm["year"]);
            obj.Price = Convert.ToDecimal(objfrm["price"]);
            obj.New = objfrm["new"].ToString() == "on";
            _UoW.Save();
            return RedirectToAction("Index");
        }
        public ActionResult Search(string key)
        {
            ViewBag.Key = "Result for " + key;
            UnitOfWork _UoW = new UnitOfWork(new CarsEntities());
            var lst = _UoW.CarsRepo.SearchByModelOrBrand(key);
            return View("Index", lst);
        }
        public ActionResult Inventory()
        {
            List<Car> lst = new List<Car>();
            if(Session["user"] == null)
                return RedirectToAction("Index");
            User objUser = (User)Session["user"];
            ViewBag.Key = "Inventory of " + objUser.Username;
            UnitOfWork _UoW = new UnitOfWork(new CarsEntities());
            lst = _UoW.InventoryRepo.GetCarsByuserID(objUser.UserID);
            return View("Index", lst);
        }
        public int AddCarToInventory(int carID)
        {
            if (Session["user"] == null)
                return 0;
            User objUser = (User)Session["user"];
            UnitOfWork _UoW = new UnitOfWork(new CarsEntities());
            _UoW.InventoryRepo.AddCarToInventory(carID, objUser.UserID);
            _UoW.Save();
            return 1;
        }
    }
}