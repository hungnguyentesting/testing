﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestProject.Models;
using TestProject.UoW;

namespace TestProject.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(string username, string pass)
        {
            UnitOfWork _UoW = new UnitOfWork(new CarsEntities());
            var objUser = _UoW.UsersRepo.GetByName(username);
            if(objUser == null)
                ViewBag.Error = "Account is not exist!";
            else
            {
                if(pass != objUser.Pass)
                    ViewBag.Error = "Wrong password!";
                else
                {
                    Session["user"] = objUser;
                    return RedirectToAction("Index", "Home");
                }
            }
            return View();
        }
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Register(string username, string pass, string confirm)
        {
            UnitOfWork _UoW = new UnitOfWork(new CarsEntities());
            var objUser = _UoW.UsersRepo.GetByName(username);
            if (objUser != null)
                ViewBag.Error = "Account is already exist!";
            else
            {
                if (pass != confirm)
                    ViewBag.Error = "Password and confirm password is not match!";
                else
                {
                    User obj = new User();
                    obj.Username = username;
                    obj.Pass = pass;
                    _UoW.UsersRepo.Add(obj);
                    _UoW.Save();
                    Session["user"] = obj;
                    return RedirectToAction("Index", "Home");
                }
            }
            return View();
        }
    }
}